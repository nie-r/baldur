import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTipoBComponent } from './header-tipo-b.component';

describe('HeaderTipoBComponent', () => {
  let component: HeaderTipoBComponent;
  let fixture: ComponentFixture<HeaderTipoBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderTipoBComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeaderTipoBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
