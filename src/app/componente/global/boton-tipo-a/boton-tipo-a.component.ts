import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-boton-tipo-a',
  templateUrl: './boton-tipo-a.component.html',
  styleUrls: ['./boton-tipo-a.component.css'],
})
export class BotonTipoAComponent {

  @Output() btnClicked = new EventEmitter<void>();

  onButtonClick() {
    this.btnClicked.emit();
  }

  onButtonClickTest() {
    console.log('BotonTipoAComponent');
  }
}
