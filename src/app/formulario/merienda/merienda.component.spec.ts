import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MeriendaComponent } from './merienda.component';
import { HeaderTipoAComponent } from 'src/app/componente/global/header-tipo-a/header-tipo-a.component';
import { BotonTipoAComponent } from 'src/app/componente/global/boton-tipo-a/boton-tipo-a.component';

describe('MeriendaComponent', () => {
  let component: MeriendaComponent;
  let fixture: ComponentFixture<MeriendaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ 
        MeriendaComponent,
        HeaderTipoAComponent,
        BotonTipoAComponent
      ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeriendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid initially', () => {
    expect(component.meriendaForm.valid).toBeFalsy();
  });

  it('should be valid after filling the form correctly', () => {
    component.meriendaForm.controls['titulo'].setValue('Test Merienda');
    component.meriendaForm.controls['detalle'].setValue('Test Detalle');
    component.meriendaForm.controls['cantidad'].setValue(5);
    component.meriendaForm.controls['precioUnitarioBS'].setValue(10);
    component.meriendaForm.controls['totalBS'].setValue(50);
    expect(component.meriendaForm.valid).toBeTruthy();
  });

  it('should show error message when trying to submit invalid form', () => {
    component.onSubmit();
    fixture.detectChanges();
    const errorMessageEl = fixture.debugElement.query(By.css('.error-message'));
    expect(errorMessageEl.nativeElement.textContent).toContain('El formulario no es válido.');
  });
});
